# Разворачиваем гитлаб и раннер к нему в докер контейнерах.
Для теста установка происходит на локальной машине через VirtualBox. Можно пропустить этот этап и сразу деплоить на сервере. 

**Для работы gitlab должно быть более 4 Гб оперативы.**

## VirtualBox
В первую очередь необходимо установить виртуальную машину в оперативкой >4гб
Видеоинструкция по установке [здесь](https://www.youtube.com/watch?v=wwflsXqMmOQ). 
Я устанавливал debian 11, поэтому на нем точно работает, с ним все по аналогии с видосом.
<details>
<summary>Настраиваем размер оперативки и пробрасываем порты</summary>
![изображение.png](img/ram.png)
![изображение.png](img/ports.png)
![изображение.png](img/ports2.png)
</details>

## Настройка ОС
Заходим через ssh (под виндой можно использовать putty) в пользователя. Работать из-под root - плохая практика. Но первоначальные настройки нужно сделать именно из-под root. Скорее всего по дефолту не получится подключиться к root из-за стандартных настроек безопасности. После подключения к обычному пользователю нужно ввести команду su и ввести пароль от root. Далее команды в этом разделе делать из-под него.
``` bash
apt update && apt upgrade -y
apt-get install passwd curl nano sudo net-tools -y
``` 
Добавляем пользователя в sudo 
Простой способ, но не всегда работает. (Нужно перелогиниться, чтобы работало.)
``` bash
/usr/sbin/usermod -a -G sudo ${YOUR_USER}
exit
``` 
~Сложный, но всегда рабочий~
``` bash
nano /etc/sudoers
``` 
~далее найти строчку (см ниже) и добавить пользователя (в примере test) в группу sudo~
``` bash
%sudo   ALL=(ALL:ALL) ALL
test ALL=(ALL) ALL
``` 
~Выходим из root. Далее работаем из-под обычного пользователя, которого добавили в sudo (далее пользователь test)~
``` bash
exit
``` 
## Docker
Команда для установки docker
``` bash
curl https://get.docker.com -o install.sh && sh install.sh
``` 
Команда для установки docker-compose
``` bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.3.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
``` 
Запуск docker без sudo
``` bash
sudo usermod -aG docker ${USER}
exit
``` 
Еще раз логинимся в пользователя. Проверяем работоспособность докера.
``` bash
docker run hello-world
``` 
## Настраиваем контейнеры
``` bash
mkdir -p ~/gitlab/{web,runner}
nano ~/gitlab/.env
``` 
Добавляем строчки, в которых указываем где будут храниться данные контейнеров
``` bash
GITLAB_WEB_ROOT=~/gitlab/web
GITLAB_WEB_SSH_HOST_PORT=23
GITLAB_RUNNER_ROOT=~/gitlab/runner
``` 
Создаем файл **docker-compose.yml**
``` bash
nano ~/gitlab/docker-compose.yml
``` 
Или создаем на своем компе и передаем его на сервер через putty (на самом деле pscp)
``` bash
pscp -i "path\to\priv\key.ppk" -r docker-compose.yml user@ip:/home/admin/gitlab
``` 
``` bash
version: '3.5'
services:
  gitlab-web:
    image: gitlab/gitlab-ce:latest
    hostname: 'localhost'
    restart: unless-stopped
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        gitlab_rails['gitlab_shell_ssh_port'] = 22
    ports:
      - "80:80"
      - "${GITLAB_WEB_SSH_HOST_PORT}:22"
    volumes:
      - ${GITLAB_WEB_ROOT}/etc/gitlab:/etc/gitlab
      - ${GITLAB_WEB_ROOT}/var/opt/gitlab:/var/opt/gitlab
      - ${GITLAB_WEB_ROOT}/var/log/gitlab:/var/log/gitlab
    networks:
      - gitlab_net

  gitlab-runner:
    image: gitlab/gitlab-runner:alpine
    restart: unless-stopped
    depends_on:
      - gitlab-web
    volumes:
      - ${GITLAB_RUNNER_ROOT}/etc/gitlab-runner:/etc/gitlab-runner
      - ${GITLAB_RUNNER_ROOT}/var/run/docker.sock:/var/run/docker.sock
    networks:
      - gitlab_net

networks:
  gitlab_net:
```
Запускаем:
``` bash
cd ~/gitlab
docker-compose up -d
``` 
Ждем 5-10 минут, пока по команде docker-compose ps в колонке STATUS у контейнера gitlab-gitlab-1 состояние running (starting) смениться на running (healthy).
Когда дождались, идем по ссылке: http://localhost  
В случае, если хотим остановить контейнер, используем:
``` bash
docker-compose stop
``` 
## Настройка GitLab web
Получаем пароль администратора (обращаем внимание на то, что необходимо подставить правильное название контейнера, в котором крутится gitlab-web (не раннер, а именно гитлаб). Название генерирует докер и делает он это в зависимости от пути по которому находится docker-compose.yml). В нашем случае это будет gitlab-gitlab-web-1
``` bash
docker exec -it gitlab-gitlab-web-1 grep 'Password:' /etc/gitlab/initial_root_password
``` 
Узнать название контейнера:
``` bash
docker-compose ps -a
``` 
Входим в учетку под логином root и паролем, полученным в прошлом шаге. Если не получается зайти, тогда останавливаем контейнеры и чистим содержимое папок на хосте (указаных в шаге **настраиваем контейнеры**). Потом перезапускаем все. 

Обязательно меняем пароль у root (http://localhost/-/profile/password/edit), так как он “протухает” через 24 часа. Можно создать несколько пользователей. Админ их может подтвердить тут: http://localhost/admin/users?filter=blocked_pending_approval.
## Настройка GitLab Runner
Заходим в GitLab, создаем проект, а в проекте выбираем "Settings - CI/CD". В разделе "Settings - CI/CD" раскрываем раздел "Runners" и в "Specific runners" узнаём токен
![изображение.png](img/runners.png)
Заходим в консоль раннера (именно раннера!), регистрируем его с полученным токеном (подставляем вместо ***).
Адрес http://gitlab-web:80  взят из docker-compose.yml. Docker сам резолвит сетевые адреса по названию контейнеров.
``` bash
docker exec -ti gitlab-gitlab-runner-1 bash

gitlab-runner register

Enter the GitLab instance URL (for example, https://gitlab.com/):
http://gitlab-web:80
Enter the registration token:
***
Enter a description for the runner:
[07d407440ddd]: runner
Enter tags for the runner (comma-separated):
runner
Enter optional maintenance note for the runner:

Enter an executor: ssh, virtualbox, docker, docker-ssh, parallels, shell, kubernetes, custom:
shell
``` 
Радуемся, мы зарегистрировали контейнер, но он еще не готов выполнять пайпы, можем увидеть это по вот такому значку 
(Вот хз, у меня сразу был зеленый. Проверить)


![изображение.png](img/trbl.png)
Исправляем это. Узнаём IP docker’a
``` bash
sudo ifconfig docker0
``` 
Запоминаем IP адрес из поля inet (у меня он 172.17.0.1, однако он может отличаться). Теперь в хост машине меняем файл.
``` bash
sudo nano ~/gitlab/runner/etc/gitlab-runner/config.toml
```
После строчки executor = "shell" добавить новую строчку с ключом clone_url. IP адрес взять из пункта выше. Должно быть так:
``` bash
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "09f7b231b2ba"
  url = "http://gitlab-web:80"
  token = "xkNUCUCyAk5yzGSTC5_4"
  executor = "shell"
  clone_url = "http://172.17.0.1"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
``` 
Выйти из режима редактирования можно через ctrl+X, он предложит сохранить - нажимаем “y”, уточнит название файла - нажимаем enter. На всякий случай перезагружаем все конфиги внутри контейнера с гитлабом (Работает и без этого, но может пригодится для чего-то другого)
``` bash
gitlab-ctl reconfigure
gitlab-ctl restart
```
Снова заходим в проект, к которому добавляли раннера:  "Settings - CI/CD". В разделе "Settings - CI/CD" раскрываем раздел "Runners", обнаруживаем зеленый кружочек около него, это значит, что он готов к работе, после этого редактируем раннер (тыкаем на карандаш)

![изображение.png](img/green.png)

После этого устанавливаем его настройки:

Run untagged jobs и protected - опционально.
**ПРОВЕРИТЬ ?оставить галки по дефолту и забить на теги?**

Добавляем в поле Tags слово runner, которое было указано по дефолту на этапе Настройка GitLab Runner при вызове команды gitlab-runner register

![изображение.png](img/settings.png)

Теперь создаем пайплайн для проекта (Ищем CI/CD на левой боковой панели будучи внутри проекта)

![изображение.png](img/pipe.png)

Создаем пайп (Файл .gitlab-ci.yml), в примере он состоял из одного этапа. Обращаем внимание на tags: если при настройке раннера мы убирали галочку с run untagged jobs то без этого тега работать ничего не будет
``` bash
stages:  
  - test

unit-test-job:   
  stage: test
  tags:
    - runner    
  script:
    - echo "Running"
```
Коммитим изменения, теперь после любого коммита будет прогоняться созданный пайп.

![изображение.png](img/pipe2.png)

![изображение.png](img/pipe3.png)

## Добавление раннера

Из под учетной записи root есть возможность присвоить любому проекту раннер.

**ПРОВЕРИТЬ ТАК МОЖЕТ СРАЗУ ЕГО ДОБАВЛЯТЬ ИЗ КОНСОЛИ?**

Теперь добавляем тот же самый раннер, но к другому проекту (можно к публичному/приватному - все-равно). Для этого идем в http://localhost/admin/runners (через учетку root), выбираем нужный раннер (у нас он один) и нажимаем на карандаш.

![изображение.png](img/pencil.png)

Слева снизу ищем нужный проект и нажимаем Enable

![изображение.png](img/history.png)

Если раннер добавился - он появится в списке слева сверху

![изображение.png](img/history2.png)

:)

---
развернуть на серве, заменить localhost

Настройка GitLab Runner у меня сразу зеленый

порты 22 и 23

**ПРОВЕРИТЬ ?оставить галки по дефолту и забить на теги?**

**ПРОВЕРИТЬ ТАК МОЖЕТ СРАЗУ ЕГО ДОБАВЛЯТЬ ИЗ КОНСОЛИ?**

Вот хз, у меня сразу был зеленый. Проверить
